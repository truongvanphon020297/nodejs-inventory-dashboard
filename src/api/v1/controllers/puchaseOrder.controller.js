let showCreatePurchaseOrderPage = (req, res) => {
    res.render("pages/purchase-order/create");
}

let showListPurchaseOrderPage = (req, res) => {
    res.render("pages/purchase-order/list");
}

let purchaseOrderController = {
    showCreatePurchaseOrderPage,
    showListPurchaseOrderPage
}

module.exports = purchaseOrderController;
