
let showHomePage = (req, res) => {
    res.render("pages/home/index");
}

let homeController = {
    showHomePage
}

module.exports = homeController;
