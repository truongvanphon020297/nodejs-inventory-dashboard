let showLoginPage = (req, res) => {
    res.render("pages/login/login");
}

let loginController = {
    showLoginPage
}

module.exports = loginController;
