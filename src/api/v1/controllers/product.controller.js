
let showListProductPage = (req, res) => {
    res.render("pages/product/list");
}

let showCreateProductPage = (req, res) => {
  res.render("pages/product/create");
}

let showProductDetailPage = (req, res) => {
  res.render("pages/product/detail");
}

let showUpdateProductPage = (req, res) => {
  res.render("pages/product/update");
}


let productController = {
  showListProductPage,
  showCreateProductPage,
  showProductDetailPage,
  showUpdateProductPage
}

module.exports = productController;
