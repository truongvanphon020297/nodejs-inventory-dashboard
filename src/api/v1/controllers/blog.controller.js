let showCreateBlog= (req, res) => {
    res.render("pages/blog/create");
}

let showListBlogPage = (req, res) => {
    res.render("pages/blog/list");
}

let blogController = {
    showCreateBlog,
    showListBlogPage
}

module.exports = blogController;
