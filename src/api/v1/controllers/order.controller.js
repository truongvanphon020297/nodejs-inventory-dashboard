let showListOrderPage = (req, res) => {
    res.render("pages/order/list");
}

let showInvoiceOrderPage = (req, res) => {
    res.render("pages/order/invoice");
}

let showCreateOrderPage = (req, res) => {
    res.render("pages/order/create");
}

let showUpdateOrderPage = (req, res) => {
    res.render("pages/order/update");
}

let orderController = {
    showListOrderPage,
    showCreateOrderPage,
    showInvoiceOrderPage,
    showUpdateOrderPage
}

module.exports = orderController;
