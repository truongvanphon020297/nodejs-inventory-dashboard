let showInventoryDetailPage = (req, res) => {
    res.render("pages/inventory/detail");
}

let showOverviewInventoryPage = (req, res) => {
    res.render("pages/inventory/overview");
}

let inventoryController = {
    showInventoryDetailPage,
    showOverviewInventoryPage
}

module.exports = inventoryController;
