let showCreateUserPage = (req, res) => {
    res.render("pages/user/create");
}

let showListUserPage = (req, res) => {
    res.render("pages/user/list");
}

let userController = {
    showCreateUserPage,
    showListUserPage
}

module.exports = userController;
