const express = require("express");
const blogController = require("../controllers/blog.controller");
const router = express.Router();

// const AuthMiddleware = require("../middlewares/Auth.middleware.js");
const homeController = require("../controllers/home.controller");
const inventoryController = require("../controllers/inventory.controller");
const loginController = require("../controllers/login.controller");
const orderController = require("../controllers/order.controller");
const productController = require("../controllers/product.controller");
const purchaseOrderController = require("../controllers/puchaseOrder.controller");
// const LoginController = require("../controllers/Login.controller");
const userController = require("../controllers/user.controller");

let initRoutes = (app) => {

    router.get("/homepage", homeController.showHomePage);
    // router.get("/login", LoginController.showLoginPage);

    // Sử dụng authMiddleware.isAuth trước những api cần xác thực
    // router.use(AuthMiddleware.hasToken);

    router.get("/users", userController.showListUserPage);
    router.get("/users/create", userController.showCreateUserPage);

    router.get("/blogs/create", blogController.showCreateBlog);
    router.get("/blogs", blogController.showListBlogPage);

    router.get("/inventories/details",inventoryController.showInventoryDetailPage);
    router.get("/inventories/overview",inventoryController.showOverviewInventoryPage);

    router.get("/login",loginController.showLoginPage);
 
    router.get("/orders/create",orderController.showCreateOrderPage);
    router.get("/orders",orderController.showListOrderPage);
    router.get("/orders/invoice",orderController.showInvoiceOrderPage);
    router.get("/orders/update",orderController.showUpdateOrderPage);

    router.get("/products",productController.showListProductPage);
    router.get("/products/create",productController.showCreateProductPage);
    router.get("/products/detail",productController.showProductDetailPage);
    router.get("/products/update",productController.showUpdateProductPage);

    router.get("/purchase-orders",purchaseOrderController.showListPurchaseOrderPage);
    router.get("/purchase-orders/create",purchaseOrderController.showCreatePurchaseOrderPage);

    return app.use("/", router);

}

 
module.exports = initRoutes;
 